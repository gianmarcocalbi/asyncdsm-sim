# AsyncDSM
Asynchronous Distributed Subgradient-Descend Method - Python simulator.

# Directory structure
- `./dataset` : contains the datasets that are stored in files and not generated on the fly (from UCL repository);
- `./distr` : contains the time distributions that are loaded from a file and not generated on the fly (one value per line);
- `./docs` : contains the documents;
- `./graphs` : contains those graphs that cannot be generated on the fly by the simulator (usually expander graphs because they require a lot of time to be computed), those files are then used by the graph module to generate graph topology objects;
- `./src` : python source code;
- `./logs` : contains the output of the experiments.

The files in the root `./` are either explained in the following or not relevant for replicating experiments.

# How to use the simulator
## Requirements
- python >=3.6;
- python packages: scipy, numpy, scikit-learn, matplotlib, ipython, jupyter, pandas, sympy, nose, termcolor, networkx, pyqt5.

Install them by running:
```bash
$ pip install -U scipy numpy scikit-learn matplotlib ipython jupyter pandas sympy nose termcolor networkx pyqt5
```
or if you're using a package manager (e.g. conda) install them with it. 

Then try to `cd` into the project root and run `$ python main.py` without arguments, if no error is printed then your environment is ready. **If the execution fails due to the lacking of some package try to install them and then relaunch the script again**.

### Required external dataset
Check inside the `./dataset` folder. There, before running experiments, be sure to have at least these two files (_if you don't, download them!_):

- `slice_localization_dataset.csv` ([download](https://archive.ics.uci.edu/ml/machine-learning-databases/00206/))
- `SUSY.csv` ([download](https://archive.ics.uci.edu/ml/machine-learning-databases/00279/))

Be careful to put inside the dataset folder the **CSV files (not zipped)** and also make sure that they are **named as above**.

# Running paper experiments
Run `$ python main.py --help` for seeing the list of the parameters accepted by the script.

```
$ python main.py -h
usage: main.py [-h] [-s SEED] [-d DISTR] [-i MAX_ITER] [-p SAVE_PLOTS]
               [tests [tests ...]]

Run test simulations

positional arguments:
  tests                 Specify the names of the test functions to run

optional arguments:
  -h, --help            show this help message and exit
  -s SEED, --seed SEED  Simulation seed
  -d DISTR, --distr DISTR
                        Time distribution (available distributions are: exp,
                        par, unif, spark, asciq)
  -i MAX_ITER, --iter MAX_ITER
                        Number of iterations to simulate
  -p SAVE_PLOTS, --plot SAVE_PLOTS
                        Save plots into log folder as simulation finishes
```

where **tests** is one of the following strings:

- `rings`: experiment of figure 1(a)
    - default iterations: 5000;
    - execution time with default iterations: 3-4h;
- `aligned-exp`: experiment of figure 1(b)
    - default iterations: 5000;
    - execution time with default iterations: 3-4h;
- `generic-exp`: experiment of figure 1(c)
    - default iterations: 5000;
    - execution time with default iterations: 4h;
- `multi-exp-2`: experiment of figure 2(a)
    - default iterations: 5000;
    - execution time with default iterations: 3-4h;
- `multi-exp-20`: experiment of figure 2(b)
    - default iterations: 5000;
    - execution time with default iterations: 4h;
- `multi-exp-100`: experiment of figure 2(c)
    - default iterations: 5000;
    - execution time with default iterations: 5h;
- `ct`: experiment with CT dataset of figure 3 a-b-c (with spark computing times), 7 (the same as fig.3) and 10 a-b-c (with asciq computing times)
    - default iterations: 2000;
    - execution time with default iterations: 9-10h;
- `susy`: experiment of figure 8 a-b-c (with spark computing times) and 11 a-b-c (with asciq computing times)
    - default iterations: 6000;
    - execution time with default iterations: 13h.

**DISTR** can be a string referring to one of the following 5 distributions (full explanation [here](#time_distr_class)):

- `exp` : exponential with rate 1;
- `par` : Pareto with shape 3 and scale 2;
- `unif` : uniform over the interval [0,2];
- `spark` : time from Spark Cluster;
- `asciq` : time from ASCI Q super computer.

If the distribution flag `-d` is not used then the _exponential distribution_ (`exp`) will be used by default.

**SEED** (the random seed) and **MAX_ITER** (amount of iterations to simulate) accept just integer values. 

By default, if not specified with their proper flags, the **SEED** is 22052010 and **SAVE_PLOTS** is set to false. **I suggest to use always** the `-p` flag to create and store plot images directly at the end of the simulation.

After the execution, a folder will be created inside `./test_log/`. If two different runs would lead to the same folder name (the folder name is auto-generated according to a pattern so that the simulation setup is understandable from the name of the output folder) then the previous folder will not be overriden, a different folder will be created named `<FOLDER_NAME>.conflict.XXX` (XXX is an integer index).

Later, if you didn't use the `-s` flag, you can still plot the results with the `plot.py` script. Refer to [this section](#Plot-a-saved-test) for that.

### Example
This command, for instance,

```bash
$ python main.py ct -d spark -s 123456 -i 100
```

will run the experiment with the CT dataset (regression), computing times from the Spark cluster, seed equal to 123456 and number of iterations equal to 100. Since the `-s` flag is not used then the plots are not saved in the output folder.

Below exactly the same experiment is run, but the plots are saved into the output folder:

```bash
$ python main.py ct -d spark -s 123456 -i 100
```
---

_Below there is a long and detailed description of all the parameters the simulator accepts, but to reproduce the experiments in the paper you don't need them, just refer the the section above instead._

# Entry point
The entry point to run experiments should be the script `main.py`, there you should setup the different experiments: usually define one function for experiment with the parameters you need to change and then call it.


# simulator.py
It contains the function `run(...)` that is the only method to call to get a full experiment. It accepts several parameters, many of them optionals, that are used to fully customize your experiment. In the following there their descriptions.

## Parameters
All parameters ending with **(!)** are mandatory to be set.

**All parameters must be explicitly set**:
```python
# CORRECT
simulator.run(
    seed=12313,
    n=100,
    # ...
)
```

```python
# WRONG
simulator.run(
    12313,
    100,
    # ...
)
```

### `seed`
- **type** : int or `None`
- **default** : `None`

Random simulation seed. If None, then the seed will be set equal to the current timestamp (in seconds). A simulation run many times with the same seed will give the same results.

### `n`
- **type** : int
- **default** : 100

Amount of executors in the simulated cluster, hence the amount of nodes of the topologies.

### `graphs` (!)
- **type** : list of strings

List of topologies to run the simulation with.

Each graph topology is thus "expressed" as a string. Each _graph string_ is converted into a **Graph object** by the function `generate_n_nodes_graphs_list(n, graphs)` within `./src/graphs.py`. 
A _graph string_ must follow the pattern `"<degree>-<graph_type>"` (**the dash is mandatory** to separate degree and graph type) where:

- `<degree>` is an integer from 1 to n-1 (even though the usage of the variable `n` should work fine, it's strongly recommended to use the integer whose n stands for instead; for example, if `n=100`, use 99 instead of `"n-1"` for the `<degree>`)
- `<graph_type>` is a string encompasses the type of the graph. The following graph types are available:
    - `"expander"` : random regular undirected graph;
    - `"cycle"` : directed cycle/ring (each node i is connected to the following d nodes where d is the `<degree>`);
    - `"undir_cycle"` : undirected cycle/ring (the same as the directed but with indirect edges);
    - `"clique"` : clique graph (**nb**: please **keep the consistency of the degree of the clique**; when using `"clique"` as graph type, be careful to always use the value of n-1 as `<degree>`);
    - `"uniform_edges"`, `"root_expander"`, `"alt_expander"` : old graph types no more used;
    - ~~`"diagonal"` : completely disconnected graph where each node has a self loop~~ (preferable to avoid it);
    - **else**: any other string will raise an exception `"Graph <graph_type> doesn't exist"`.

**NB**: all graph types specified with degree equals to the value of n-1 will lead to a clique, however, If you'd like to have a clique, always opt for the `"clique"` graph type rather than using another graph type with `<degree>` equals to the value of n-1.

**Example**.
```python
# the following is a valid "graphs" parameter for the simulator.run(...) function with n=100
graphs = [
    '1-cycle',
    '2-expander'
    '5-expander',
    '77-undir_cycle'
    '99-clique'
]
```

### `n_samples`
- **type** : int
- **default** : `None`

Total number of samples in the generated dataset. For some datasets `n_samples` is ignored (further details in the dataset parameter description).

### `n_features`
- **type** : int
- **default** : 100

Number of feature each sample will have. For some dataset this parameter is ignored.

### `dataset` (!)
- **type** : str

Dataset label. Can be: 

- "reg": general customizable linear regression dataset;
- "unireg": unidimensional regression;
- "svm": multidimensional classification problem;
- "unisvm": unidimensional dataset that changes with topology spectral gap;
- "skreg" : regression dataset from sklearn library,
- "sloreg" and "susysvm" from UCI's repository: for these two it's required to download their datasets and place inside the dataset folder.


### `smv_label_flip_prob`
- **type** : float
- **default** : 0.0

Probability that a "boolean" label is flipped in a svm dataset (from +1 to -1 or viceversa). This parameter is ignored in any other dataset. It's a kind of noise added in the dataset.

### `error_mean`
- **type** : float
- **default** : 0.0

Mean of the gaussian noise to introduce in a regression dataset. Ignored for other dataset types.

### `error_std_dev`
- **type** : float
- **default** : 0.0

Standard deviation of the gaussian noise introduced in regression datasets. Ignored in other dataset types.

### `node_error_mean`
- **type** : float
- **default** : 0.0

Mean of the per-node gaussian noise introduced in each node sample. Be careful: don't touch this parmeter if working with SVMs, this can change values of labels (the label would be changed from +1/-1 to \$+1.??/-1.??\$, etc.).

### `node_error_std_dev`
- **type** : float
- **default** : 0.0

Standard deviation of the per-node noise introduced in each node sample.
_Be careful because if used with SVM this can change values of labels!_

**Q: What is the difference between "error" and "node_error"?** 
**A**: Error is a gaussian noise added to each sample of the dataset, most likely each sample will have a different noise value. While, the node_error, is the same for all samples within a node. For instance if `node_error=0.5` for the node i, then all samples of the node i will have added 0.5 to their values.


### `starting_weights_domain` (!)
- **type** : list of 2 floats

It's a list in the form of `[<a>,<b>]` where **a>=b** (!!!). 
The value of each component of the starting weight/parameter vector **w** will be determined by uniformly randomly picking a value between a and b. If a=b then all components will have the same value.

### `max_iter`
- **type** : int
- **default** : `None`

Maximum number of gradient descent iteration: when all nodes have completed at least `max_iter` iterations then the simulation stops. If `None` then the simulation never stops due to the number of iterations.

### `max_time`
- **type** : float
- **default** : `None`

Maximum time value after which the simulation is stopped. If `None` then the simulation never stops due to the elapsed time.

### `epsilon` (tolerance)
- **type** : float
- **default** : 0.0

Accuracy threshold for objective function. When the value of the objective function goes below `epsilon` the simulation is stopped.

**NB**: if you define none of the previous three parameter, for sure the computation will never stop. Setting a parameter `epsilon`>0 does not guarantee that the computation will finish. I'd suggest to set always at least one between `max_iter` and `max_time`.

### `method`
- **type** : str
- **default** : "classic"

Gradient descent optimization method used during the simulation. The following methods are available:

- `"classic"` : classic gradient descent, batch is equal to the whole dataset;
- `"stochastic"` : stochastic gradient descent;
- `"batch"` : batch gradient descent;
- `"subgradient"` : subgradient projected gradient descent;
- `"dual_averaging"` : dual averaging method.

### `batch_size`
- **type** : int
- **default** : 20

Useful only for batch gradient descent, is the size of the mini-batch. For other methods type it's ignore.

### `alpha` (!) 
- **type** : float or list of float

Learning rate (gradient step size) constant coefficient. If a list is considered it should 

### `learning_rate`
- **type** : str
- **default** : `"constant"`

Learning rate _behaviour_. The following are available:

- `'constant'` : the step size never changes during the simulation (it is equal to alpha);
- `'root_decreasing'` : the step size is `alpha * 1/math.sqrt(K)` where K = #iter.

### `dual_averaging_radius`
- **type** : int
- **default** : 10

Radius of the projection on the feasible set for the projectore operator used only with the dual_averaging method.

### `time_distr_class`
- **type** : object
- **default** : `statistics.ExponentialDistribution`

Class of the random time distribution used for randomly picking the time taken by a node when carrying out one iteration. It must be one of the classes extending the abstract class `DistributionAbstract`, such classes are contained in `./src/statistic.py` and they are:

- `statistics.ExponentialDistribution`;
- `statistics.UniformDistribution`;
- `statistics.Type2ParetoDistribution`;
- `statistics.SparkRealTimings`: samples from a spark cluster experiment with the gradient descent method;
- `statistics.CustomRealTimings`: samples from a the ASCI Q supercomputer.

### `time_distr_param`
- **type** : list _or list of lists_
- **default** : `[1]`

List of the parameters of the `time_distr_class`. _This parameter supports a more advanced setup by setting it equal to a list of lists but it is not needed to our last experiments._ The list depends on the chosen `time_distr_class`:

- `[<l>]` where `<l>` rate of the exponential if `time_distr_class = statistics.ExponentialDistribution`;
- `[<a>, <b>]` if `time_distr_class = statistics.UniformDistribution`;
- `[<shape>, <scale>]` if `time_distr_class = statistics.Type2ParetoDistribution`;
- for the other distributions the parameter `time_distr_param` is ignored.

### `time_const_weight`
- **type** : float
- **default** : 0.0

Weight assigned to constant part of the computation time. The computation time is then calculated as $`T_u (t) = E\mathbb{[}X_u\mathbb{]} * c + (1-c) * X_u(t)`$ where $`X_u`$ is a random variable distributed as selected as per the `time_distr_class` parameter.

### `obj_function`
- **type** : str
- **default** : `"mse"`

Identifier of the objective function (one of those declared in `./src/mltoolbox/metrics.py`). Available objective functions:

- "mse" : mean squared error;
- "hinge_loss" : hinge loss;
- "cont_hinge_loss" : continuous hinge loss (without the max operator);
- "edgy_hinge_loss" (obsolete) and "score" are not supposed to be used as objective function

**Use mse for regressions and any hinge_loss for classifications**.

### `average_model_toggle`
- **type** : bool
- **default** : False

If True then the average of parameter vector over already elapsed iteration up to the current iteration k is used istead of just x(k).

### `metrics`
- **type** : list of str
- **default** : `[]`

List of additional metrics to compute (objective function is automatically added to this list). The metric availables as the same listed in the description of the `obj_function` parameter.

### `metrics_type`
- **type** : int
- **default** : 0

Decide how metrics (also objective function) are computed:

- 0 : metrics are computed over the whole dataset using model W equal to the avg of nodes local models for the same iteration (default behavior);
- 1 : metrics are computed as the average of local nodes metrics (for instance each node computes the value of the obj_function with its local model / dataset / etc. and then the resulting overall metrics is the average of local obj_functions, the same for all other metrics in `metrics`);
- 2 : metrics are computed over the whole dataset using the model only from metrics_nodes (see below).

### `metrics_nodes`
- **type** : str _or int or list of int_
- **default** : `"all"`

This parameter is strictly related to `metrics_type` parameter.
This parameter is taken into account only when `metrics_type` is equal to 2, otherwise it is ignored. 
The admitted values are:

- `'all'` : use all nodes (setting `metrics_type = 2` and `metrics_nodes = 'all'` is equivalent to set `metrics_type = 0`);
- `'best'` : use the best node (smallest obj function);
- `'worst'` : use the worst node (largest obj function);
- list of int : use only the nodes specified in the list;
- int : it is put into a list and treated as [int].

### `shuffle`
- **type** : bool
- **default** : True

If True the dataset is shuffled before being split and assigned to nodes, otherwise the dataset mantains the original ordering.

### `save_test_to_file`
- **type** : bool
- **default** : False

If True the test is saved to specified folder, otherwise it is stored into the `./logs/temp` folder named as the creation time timestamp.

### `test_folder_name_struct`
- **type** : list
- **default** : 
```python
[
    '',
    'shuffle',
    'w_domain',
    'metrics',
    'dataset',
    'distr',
    'error',
    'nodeserror',
    'alpha',
    'nodes',
    'samp',
    'feat',
    'time',
    'iter',
    'c',
    'method'
]
``` 

You can skip this parameter since it is optional. If you leave it as it is the output folder will be named so that you can recognize it immediately among others originated from a different setup.   

### `test_parent_folder`
- **type** : str
- **default** : `""`

Parent test folder: the test output files will be located in `./logs/<test_parent_folder>/<test_folder_name>` where `<test_folder_name>` is created as described in the previous parameter. _Can be more than one-folder-deep!_

### `save_plot_to_file`
- **type** : bool
- **default** : False

If True, when the simuation will finish, plots will be saved into `./logs/.../<test_folder_name>/plots/` folder.

### `instant_plot`
- **type** : bool
- **default** : False

If True plots will be prompted upon finishing simulation. 
**Be careful since it will pause the thread**! If you planned several simulations in a row, plotting upon finishing each test will require user input (to close the opened plot windows) to proceed.

### `plots`
- **type** : list of str
- **default** : `[]`

List of plot names to save (if `save_plot_to_file == True`) and/or to prompt (if `instant_plot == True`) upon finishing the simulation. 
The available plots are:

- `iter_time` : iterations over time;
- `<metrics_name>_iter` : the entered `<metrics_name>` over iterations;
- `<metrics_name>_time` : the entered `<metrics_name>` over time.

So, for instance, if you use mse metrics then `mse_iter` and `mse_time` are valid plots.

### `verbose_main`
- **type** : int
- **default** : 0

Verbose policy in simulator.py script:

- <0 : no print at all except from errors (unsafe).
-  0 : default messages;
-  1 : verbose + default messages
-  2 : verbose + default messages + input required to continue after each message (simulation will be paused after each message and it will be required to press ENTER to go on, useful for slow debugging).


## Other parameters
You shouldn't need them.

### `spectrum_dependent_learning_rate` (old)
- **type** : bool
- **default** : False

If True the learning rate is also multiplied by `math.sqrt(spectral_gap)`, so it can be different for different graphs (_this paramater has been used for some tests in the past, right now it is not supposed to be useful anymore_).

### `real_y_activation_func` (obsolete)
- **type** : function
- **default** : `None`

Activation function applied on real_y calculation.

### `real_metrics` (obsolete)
- **type** : list of str
- **default** : []

List of real metrics to compute (with regards to the real noiseless model).

### `real_metrics_toggle` (obsolete)
- **type** : bool
- **default** : False

If False real metrics are not computed (keep it set to False to speed up the computation).

### `plot_global_w`
- **type** : bool
- **default** : `False`

If True global W will be prompted after finishing simulation. 
This plot is never automatically saved, save it by yourself if you need to keep it.

### `plot_node_w`
- **type** : list or `False`
- **default** : `False`

List of nodes of which to plot the weight vector history. If False nothing will be plotted.

### `verbose_cluster`
- **type** : int
- **default** : 0

Verbose policy in cluster.py script. See verbose_main.

### `verbose_node`
- **type** : int
- **default** : 0

Verbose policy in node.py script. See verbose_main.

### `verbose_task`
- **type** : int
- **default** : 0

Verbose policy in tasks.py script. See verbose_main.

### `verbose_plotter`
- **type** : int
- **default** : 0

Verbose policy in plotter.py script. See verbose_main.

---

The `run` method has returns the path where test logs have been saved.

## Output
As introduced before, the output of the test/experiment will be a folder in `./test_log/` named as described from the test_folder_name_struct parameter. Inside such folder you will find many files (logs) and, if `save_test_to_file` has been set to True, you will find also a plot folder containing plot images.

# Plot a saved test
Use the `plot.py` script.

This is the description of its usage:
```bash
usage: plot.py [-h] [-p PLOTS [PLOTS ...]] [-f FOLDER_PATH] [-s]

Plotter script

optional arguments:
  -h, --help            show this help message and exit
  -p PLOTS [PLOTS ...], --plots PLOTS [PLOTS ...]
                        List of plots to create
  -f FOLDER_PATH, --folder-path FOLDER_PATH
                        Test folder from which load logs to
  -s, --save            Specify whether save to file or not
```

Most straightforward way to get some plot: 

1. cd into the project root folder;
2. type `$ python plot.py` followed by 
    - `-f <FOLDER_PATH>` where FOLDER_PATH targets the test folder of which you want to draw plots;
    - `-p <PLOT_NAME_1> ...` that is the flag `-p` followed by a list of plot names separated by a space (see [plots parameter](#plots) to know how to well format a plot name); if a plot name refers to a metrics not calculated during the simulation then such plot is skipped without raising any error;
    - optionally use the flag `-s` at the end (without arguments) to save files in the test folder: if you use this flag the plots will not be prompted immediately but will be saved to folder only (this is a desired behavior and not a bug), otherwise no plot will be saved but they will be just shown immediately.
    
**Example** 
```bash
$ python plot.py -f ./test_log/test_1undir_cycle_eigvecsvm_C0.1alpha_100n -p hinge_loss_iter hinge_loss_time -s
```

This command will create plot images into `./test_log/test_1undir_cycle_eigvecsvm_C0.1alpha_100n/plots/` for the hinge loss over time and hinge loss over iter.

**NOTA BENE**: plotting require a display (like Xorg or wayland on linux) to work. If you're using a normal desktop PC you should not be impacted by this requirement.