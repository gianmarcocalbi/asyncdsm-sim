import glob
import os
import datetime
import simulator
import argparse
import math
import numpy as np

from src.utils import load_test_logs
from src.plotter import Plotter
from matplotlib import pyplot as plt
from src import statistics
from main import get_graphs

"""
This script is supposed to be used to determine the best (HIGHEST) learning rate
for topologies of an experiment.
"""
SLOPE_CURVE_TOGGLE = False

SMALL_SIZE = 10
MEDIUM_SIZE = 12
BIGGER_SIZE = 14
BBOX_INCHES = None
INSERT_FONT_SIZE = 'small'
AXIS_FONT_SIZE = 'large'
AXIS_FONT_SIZE_BIGGER = 'x-large'
LEGEND_FONT_SIZE = 'medium'

plt.rc('font', size=SMALL_SIZE)  # controls default text sizes
plt.rc('axes', titlesize=SMALL_SIZE)  # fontsize of the axes title
plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
plt.rc('xtick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('ytick', labelsize=SMALL_SIZE)  # fontsize of the tick labels
plt.rc('legend', fontsize=SMALL_SIZE)  # legend fontsize
plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

def generate_alpha_list():
    alpha_list = []
    for digit in range(1, 101):  # 1,101
        alpha_list.append(1e-5 * digit)
    return alpha_list


def tune_graph(graph, alpha_list, seed=None, **simulator_args):
    # the curve is the error (from the obj function) after the first iteration
    # over alpha (the learning rate) used for that experiment
    curve_points = []

    # prev_error = -math.inf
    # prev_alpha = 0
    for alpha in alpha_list:
        test_path = simulator.run(
            seed=seed,
            alpha=alpha,
            max_iter=1,
            graphs=[graph],
            max_time=None,
            epsilon=None,
            save_test_to_file=False,
            instant_plot=False,
            save_plot_to_file=False,
            plot_global_w=False,
            plot_node_w=False,
            **simulator_args,
        )

        test_logs = load_test_logs(test_path, return_setup=False)
        error = test_logs['metrics']['mse'][graph][1]
        curve_points.append((alpha, error))
        print("alpha={} -> error={}".format(alpha, error))

    return curve_points


def generate_slope_points_from_curve(curve_points):
    curve_slope_points = []
    prev_error = -math.inf
    prev_alpha = 0
    for p in curve_points:
        curr_alpha = p[0]
        curr_error = p[1]
        slope = (curr_error - prev_error) / (curr_alpha - prev_alpha)
        curve_slope_points.append((curr_alpha, slope))
        prev_error = curr_error
        prev_alpha = curr_alpha

    curve_slope_points[0] = curve_slope_points[1]


def tune_all(graph_list, seed=None, **simulator_args):
    global SLOPE_CURVE_TOGGLE
    dest_folder = "./test_log/paper3/lr_tuning/" + \
                  datetime.datetime.now().strftime('%y-%m-%d_%H.%M.%S.%f')
    if not os.path.exists(dest_folder):
        os.makedirs(dest_folder)
    else:
        raise Exception("Destination folder for logs already exists")

    alpha_list = generate_alpha_list()

    for graph in graph_list:
        curve_points = tune_graph(graph, alpha_list, seed, **simulator_args)
        np.savetxt(
            os.path.join(dest_folder, "lr-tuning-{}.txt.gz".format(graph)),
            np.array(curve_points),
            delimiter=','
        )
        if SLOPE_CURVE_TOGGLE:
            curve_slope_points = generate_slope_points_from_curve(curve_points)
            np.savetxt(
                os.path.join(dest_folder, "lr-tuning-{}-slope.txt.gz".format(
                    graph)),
                np.array(curve_slope_points),
                delimiter=','
            )

    np.savetxt(
        os.path.join(dest_folder, "learning-rates.txt.gz"),
        np.array(alpha_list),
        delimiter=','
    )


def plot_all_graph_tunings(folder):
    alpha_list = None
    graph_types = {}
    graphs = {}
    plt.xlabel('Learning rate (' + r'$\eta$' + ')', fontsize=AXIS_FONT_SIZE)
    #plt.ylabel(r'$F(\hat w_j(k))$', fontsize=AXIS_FONT_SIZE_BIGGER)
    plt.ylabel('Mean Squared Error', fontsize=AXIS_FONT_SIZE)
    # plt.yscale('linear')
    plt.xscale('log')
    for path in glob.iglob(os.path.join(folder, "*.txt.gz")):
        if 'learning-rates.txt.gz' in path:
            alpha_list = np.loadtxt(path)
        elif os.path.isfile(path):
            degree = int(os.path.basename(path).split('-')[2])
            graph_types[degree] = os.path.basename(path).split('-')[3].split('.')[0]
            graphs[degree] = []
            for s in np.loadtxt(path, str):
                x, y = s.split(",")
                graphs[degree].append((float(x), float(y)))

    int_keys = [int(k) for k in graphs.keys()]
    int_keys.sort()
    for degree in int_keys:
        markersize = 0
        marker = 'o'
        zorder=1
        if degree == 2:  # and distr == 'par':
            markersize = 6
            marker = 'x'
            zorder = 50
        elif degree == 99:
            markersize = 5
            marker = 'o'
            zorder = 80

        plt.plot(
            [p[0] for p in graphs[degree]],
            [p[1] for p in graphs[degree]],
            label=str(degree),
            markevery=0.05,
            markersize=markersize,
            marker=marker,
            color=Plotter.get_color_from_graph_degree(degree, 100),
            zorder=zorder
        )

    plt.legend(title="Degree (d)", fontsize=LEGEND_FONT_SIZE, fancybox=True)
    fig = plt.gcf()
    plt.show()
    fig.savefig('./test_log/paper3/figures/lr-tuning-plot.png')
    print('Figure saved to ' + './test_log/paper3/figures/lr-tuning-plot.png')
    plt.close()


def tune_graph_old(graph=None, seed=None, **simulator_args):
    # the curve is the error (from the obj function) after the first iteration
    # over alpha (the learning rate) used for that experiment
    curve_points = []
    curve_slope_points = []
    alpha_list = []

    for digit in range(1, 101):
        alpha_list.append(1e-5 * digit)

    # prev_error = -math.inf
    # prev_alpha = 0
    for alpha in alpha_list:
        test_path = simulator.run(
            seed=seed,
            alpha=alpha,
            max_iter=1,
            graphs=[graph],
            max_time=None,
            epsilon=None,
            save_test_to_file=False,
            instant_plot=False,
            save_plot_to_file=False,
            plot_global_w=False,
            plot_node_w=False,
            **simulator_args,
        )

        test_logs = load_test_logs(test_path, return_setup=False)
        error = test_logs['metrics']['mse'][graph][1]
        curve_points.append((alpha, error))
        print("alpha={} -> error={}".format(alpha, error))

    if True:
        prev_error = -math.inf
        prev_alpha = 0
        for p in curve_points:
            curr_alpha = p[0]
            curr_error = p[1]
            slope = (curr_error - prev_error) / (curr_alpha - prev_alpha)
            curve_slope_points.append((curr_alpha, slope))
            prev_error = curr_error
            prev_alpha = curr_alpha

        curve_slope_points[0] = curve_slope_points[1]

    plt.title(graph + ' LR tuning')
    plt.xlabel('Alpha')
    plt.ylabel('Error')
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
    plt.xscale('log')

    # curve_points.sort(key=lambda p: p[0])
    lx = [p[0] for p in curve_points]
    ly = [p[1] for p in curve_points]
    plt.plot(
        lx,
        ly,
        markersize=1,
        marker='o',
    )
    fig = plt.gcf()
    plt.show()

    fig_path_1 = './docs/LR-tuning-out/lr-tuning-{}'.format(graph)
    fig_path_2 = './docs/LR-tuning-out/lr-tuning-{}-slope'.format(graph)
    index = 0
    if os.path.exists(fig_path_1 + ".png"):
        fig_path_1 = "./docs/LR-tuning-out/lr-tuning-{}.{}".format(
            graph, index)
        while os.path.exists(fig_path_1 + ".png"):
            index += 1
            fig_path_1 = "./docs/LR-tuning-out/lr-tuning-{}.{}".format(
                graph, index)
            fig_path_2 = './docs/LR-tuning-out/lr-tuning-{}-slope.{}'.format(
                graph, index)

    fig.savefig(fig_path_1 + ".png")
    plt.close()

    if True:
        plt.title(graph + ' LR tuning - SLOPE')
        plt.xlabel('Alpha (LR)')
        plt.ylabel('Error slope')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        # plt.xscale('log')

        curve_slope_points.sort(key=lambda p: p[0])
        lx = [p[0] for p in curve_slope_points]
        ly = [p[1] for p in curve_slope_points]
        print("Steepest down-slope is given by alpha={}".format(
            lx[np.argmin(ly)]))
        plt.plot(
            lx,
            ly,
            markersize=1,
            marker='o',
        )
        fig = plt.gcf()
        plt.show()
        fig.savefig(fig_path_2 + ".png")
        plt.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Run cyclic LR tuner'
    )
    """
    parser.add_argument(
        'command',
        type=str,
        nargs='?',
        help='Give a root command to the script (available commands: plot-all, '
             'tune)',
        default=[]
    )
    """

    parser.add_argument(
        '-d', '--degree',
        action='store',
        required=False,
        help='Graph degree',
        dest='degree',
        type=int,
        default=-1
    )

    args = parser.parse_args()

    if args.degree >= 2:
        tune_graph(
            "{}-expander".format(args.degree),
            generate_alpha_list(),
            seed=22052010,
            n=100,
            n_samples=52000,
            dataset='sloreg',
            starting_weights_domain=[2, 3],
            learning_rate='constant',
            spectrum_dependent_learning_rate=False,
            time_distr_class=statistics.SparkRealTimings,
            time_distr_param=[],
            obj_function='mse',
            metrics=[],
            metrics_type=2,
            metrics_nodes='worst',
            shuffle=True
        )
        # plot_graph_tuning()
    else:
        """
        tune_all(
            get_graphs('expander', 100),
            seed=22052010,
            n=100,
            n_samples=52000,
            dataset='sloreg',
            starting_weights_domain=[2, 3],
            learning_rate='constant',
            spectrum_dependent_learning_rate=False,
            time_distr_class=statistics.SparkRealTimings,
            time_distr_param=[],
            obj_function='mse',
            metrics=[],
            metrics_type=2,
            metrics_nodes='worst',
            shuffle=True
        )
        """
        plot_all_graph_tunings("./test_log/paper3/lr_tuning/"
                               "19-01-22_11.37.59.433330/")
