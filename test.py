from main import *
import time

def run(test_id, degree, alpha):

    # test #2
    if test_id == '2':
        test_on_eigvecsvm_dataset(
            seed=22052010,
            n=100,
            distr='exp',
            metrics_nodes='worst',
            alert=False,
            graphs_list=[str(degree) + '-expander'],
            alpha=alpha
        )

    if test_id == '6reg':
        """
        d=2  -> a=0.000475
        d=3  -> a=0.0004 (0.00045 diverges)
        d=4  -> a=0.0003
        d=8  -> a=0.0004
        d=16 -> a=0.000
        d=32 -> a=
        d=64 -> a=
        d=99 -> a=0.0006
        """
        #seed=int(time.time())
        test_on_ctreg_dataset(
            seed=22052010, n=100, alpha=alpha,
            graphs=[str(degree) + '-expander'])


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Run test simulations'
    )

    parser.add_argument(
        '-t', '--test',
        action='store',
        required=True,
        help='Test id',
        dest='test',
        type=str
    )

    parser.add_argument(
        '-a', '--alpha',
        action='store',
        required=True,
        help='Test alpha',
        dest='alpha',
        type=float
    )

    parser.add_argument(
        '-d', '--degree',
        action='store',
        required=True,
        help='Test graph degree',
        dest='degree',
        type=int
    )

    args = parser.parse_args()

    run(args.test, args.degree, args.alpha)
