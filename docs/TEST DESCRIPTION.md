
# Test #1 (toy example)
## Setup
```python
if core == 0:
    test_on_eigvecsvm_dataset(seed=22052010, graph_type='expander', n=100, distr='par', metrics_nodes='worst',
        alert=False)
elif core == 1:
    test_on_eigvecsvm_dataset(seed=22052010, graph_type='expander', n=100, distr='unif', metrics_nodes='worst',
        alert=False)
elif core == 2:
    test_on_eigvecsvm_dataset(seed=22052010, graph_type='expander', n=100, distr='exp', metrics_nodes='worst',
        alert=False)
elif core == 3:
    test_on_eigvecsvm_dataset(seed=22052010, graph_type='undir_cycle', n=100, distr='par', metrics_nodes='worst',
        alert=False)
elif core == 4:
    test_on_eigvecsvm_dataset(seed=22052010, graph_type='undir_cycle', n=100, distr='unif', metrics_nodes='worst',
        alert=False)
elif core == 5:
    test_on_eigvecsvm_dataset(seed=22052010, graph_type='undir_cycle', n=100, distr='exp', metrics_nodes='worst',
        alert=False)
```

- **dataset** : uni
- 

# Test #2 (toy example)
The same as test #1 but with random graphs

# Test #3 (toy example)
Same dataset as in test #2 but now the algorithm runs on a different random graph the same for all the datasets and different from any graph considered to generate datasets 