import argparse

import simulator
from src import statistics
from src.utils import *


def run(tests, seed, max_iter, distr, save_plots):
    if seed is None:
        seed = 22052010
    time_distr_class = {
        'exp': statistics.ExponentialDistribution,
        'unif': statistics.UniformDistribution,
        'par': statistics.Type2ParetoDistribution,
        'spark': statistics.SparkRealTimings,
        'asciq': statistics.CustomRealTimings
    }[distr]
    time_distr_param = {
        'exp': [[1]],
        'unif': [[0, 2]],
        'par': [[3, 2]],
        'spark': [],
        'asciq': []
    }[distr]

    if len(tests) == 0:
        print(col("Nothing to run, anyway your environment looks well set up.",
        'green'))

    for test in tests:
        # TEST 1
        if test == 'rings':
            test_on_eigvecsvm_dataset(
                seed=seed,
                graph_type='undir_cycle',
                n=100,
                max_iter=max_iter,
                time_distr_class=time_distr_class,
                time_distr_param=time_distr_param,
                metrics_nodes='worst',
                save_plots=save_plots,
                alert=True
            )

        elif test == 'aligned-exp':
            test_on_eigvecsvm_dataset(
                seed=seed,
                graph_type='expander',
                n=100,
                max_iter=max_iter,
                time_distr_class=time_distr_class,
                time_distr_param=time_distr_param,
                metrics_nodes='worst',
                save_plots=save_plots,
                alert=True
            )

        elif test == 'generic-exp':
            test_on_eigvecsvm_dataset(
                seed=seed,
                graph_type='expander',
                n=100,
                max_iter=max_iter,
                time_distr_class=time_distr_class,
                time_distr_param=time_distr_param,
                metrics_nodes='worst',
                save_plots=save_plots,
                alt_exp=True,
                alert=True
            )

        elif 'multi-exp-' in test:
            n_samples = int(test.split('-')[-1])
            test_on_multieigvecsvm_dataset(
                seed=seed,
                n=100,
                n_samples=n_samples,
                max_iter=max_iter,
                time_distr_class=time_distr_class,
                time_distr_param=time_distr_param,
                save_plots=save_plots,
                alert=True
            )

        elif test == 'ct':
            test_on_ctreg_dataset(
                seed=seed,
                n=100,
                max_iter=max_iter,
                alpha=3e-4,
                time_distr_param=time_distr_param,
                time_distr_class=time_distr_class,
                save_plots=save_plots,
                alert=True
            )

        elif test == 'susy':
            test_on_susysvm_dataset(
                seed=seed,
                n=100,
                max_iter=max_iter,
                time_distr_param=time_distr_param,
                time_distr_class=time_distr_class,
                save_plots=save_plots,
                alert=True
            )


"""
For a full description of tests rely on the documentation and not
too much on the comments here inside.
"""


def test_on_susysvm_dataset(
        seed=None,
        n=100,
        alpha=5e-2,
        graphs=None,
        max_iter=None,
        time_distr_class=None,
        time_distr_param=None,
        alert=False,
        save_plots=False
):
    # linear_regression result = 67.30972320004327

    if max_iter is None:
        max_iter = 6000

    if alert:
        print('test_on_susysvm_dataset()')
        print('n={}, distr={}, alpha={}'.format(
            col(n, 'yellow'),
            col(time_distr_class.shortname, 'cyan'),
            col(alpha, 'green')))
        input("click [ENTER] to continue or [CTRL]+[C] to abort")
        print(col("Simulation is preparing to start", 'green'))

    if graphs is None:
        graphs = get_graphs('expander', n)

    simulator.run(
        seed=seed,
        n=n,
        n_samples=500000,
        graphs=graphs,
        dataset='susysvm',
        starting_weights_domain=[-0.5, 2],
        max_iter=max_iter,
        max_time=None,
        alpha=alpha,
        learning_rate='constant',
        spectrum_dependent_learning_rate=False,
        time_distr_class=time_distr_class,
        time_distr_param=time_distr_param,
        obj_function='hinge_loss',
        average_model_toggle=True,
        metrics=[],
        metrics_type=0,
        metrics_nodes='all',
        shuffle=True,
        save_test_to_file=True,
        test_folder_name_struct=[
            'test7',
            'dataset',
            # 'w_domain',
            'nodes',
            'distr',
            'metrics',
            'alpha',
            'samp',
            # 'feat',
            'time',
            'iter'
        ],
        test_parent_folder="",
        instant_plot=False,
        plots=['iter_time', 'hinge_loss_iter', 'hinge_loss_time'],
        save_plot_to_file=save_plots,
        plot_global_w=False,
        plot_node_w=False
    )


def test_on_ctreg_dataset(
        seed=None,
        n=100,
        alpha=3e-4,
        graphs=None,
        max_iter=None,
        time_distr_class=None,
        time_distr_param=None,
        alert=False,
        save_plots=False
):
    # linear_regression result = 67.30972320004327

    if max_iter is None:
        max_iter = 2000

    if alert:
        print('test_on_ctreg_dataset()')
        print('n={}, distr={}, alpha={}'.format(
            col(n, 'yellow'),
            col(time_distr_class.shortname, 'cyan'),
            col(alpha, 'green')))
        input("click [ENTER] to continue or [CTRL]+[C] to abort")
        print(col("Simulation is preparing to start", 'green'))

    if graphs is None:
        graphs = get_graphs('expander', n)

    simulator.run(
        seed=seed,
        n=n,
        n_samples=52000,
        graphs=graphs,
        dataset='sloreg',
        starting_weights_domain=[2, 3],
        max_iter=max_iter,
        max_time=None,
        alpha=alpha,
        learning_rate='constant',
        spectrum_dependent_learning_rate=False,
        time_distr_class=time_distr_class,
        time_distr_param=time_distr_param,
        obj_function='mse',
        metrics=[],
        metrics_type=2,
        metrics_nodes='worst',
        shuffle=True,
        save_test_to_file=True,
        test_folder_name_struct=[
            'test6',
            'dataset',
            'nodes',
            'distr',
            'metrics',
            'alpha',
            'samp',
            'time',
            'iter'
        ],
        test_parent_folder="",
        instant_plot=False,
        plots=['iter_time', 'mse_iter', 'mse_time'],
        save_plot_to_file=save_plots,
        plot_global_w=False,
        plot_node_w=False
    )


"""
def test5_on_multieigvecsvm_dataset(
        seed=None,
        n=100,
        k=16,
        n_samples=2,
        distr='par',
        metrics_nodes='all',
        max_iter=5000,
        alpha=0.1,
        time_distr_class=None,
        time_distr_param=None,
        save_plots=False,
        alert=False
):
    if alert:
        print('test_exp_on_unisvm_dataset()')
        print(
            'n={}, distr={}, metrics_nodes={}'.format(n, distr, metrics_nodes))
        input("click [ENTER] to continue or [CTRL]+[C] to abort")
        print(col("Simulation is preparing to start", 'green'))

    if metrics_nodes in ['worst', 'all']:
        metrics_type = {'worst': 2, 'all': 0, 'best': 2}[metrics_nodes]
    else:
        metrics_type = 2

    simulator.run(
        seed=seed,
        n=n,
        n_samples=n_samples,
        graphs=get_graphs('expander', n),
        dataset='{}-multieigvecsvm'.format(k),
        starting_weights_domain=[1, 1],
        smv_label_flip_prob=0.00,
        max_iter=max_iter,
        max_time=None,
        alpha=alpha,
        learning_rate='constant',
        spectrum_dependent_learning_rate=False,
        time_distr_class=time_distr_class,
        time_distr_param=time_distr_param,
        obj_function='hinge_loss',
        epsilon=-math.inf,
        average_model_toggle=True,
        metrics=[],
        metrics_type=metrics_type,
        metrics_nodes=metrics_nodes,
        shuffle=False,
        save_test_to_file=True,
        test_folder_name_struct=[
            'test5',
            'dataset',
            'samp',
            'alpha',
            'nodes',
            # 'shuffle',
            'w_domain',
            'distr',
            # 'time',
            'iter',
            # 'metrics'
        ],
        test_parent_folder="",
        instant_plot=False,
        plots=['iter_time', 'hinge_loss_iter', 'hinge_loss_time'],
        save_plot_to_file=save_plots,
        plot_global_w=False,
        plot_node_w=False
    )
"""


def test_on_multieigvecsvm_dataset(
        seed=None,
        n=100,
        n_samples=2,
        max_iter=None,
        alpha=0.1,
        time_distr_class=None,
        time_distr_param=None,
        save_plots=False,
        alert=False
):
    if max_iter is None:
        max_iter = 5000

    if alert:
        print('test_on_multieigvecsvm_dataset()')
        print('n={}, n_samples={}, distr={}, alpha={}'.format(
            col(n, 'yellow'),
            col(n_samples, 'red'),
            col(time_distr_class.shortname, 'cyan'),
            col(alpha, 'green')))
        input("click [ENTER] to continue or [CTRL]+[C] to abort")
        print(col("Simulation is preparing to start", 'green'))

    simulator.run(
        seed=seed,
        n=n,
        n_samples=n_samples,
        graphs=get_graphs('expander', n),
        dataset='multieigvecsvm',
        starting_weights_domain=[1, 1],
        smv_label_flip_prob=0.00,
        max_iter=max_iter,
        max_time=None,
        alpha=alpha,
        learning_rate='constant',
        spectrum_dependent_learning_rate=False,
        time_distr_class=time_distr_class,
        time_distr_param=time_distr_param,
        obj_function='cont_hinge_loss',
        epsilon=-math.inf,
        average_model_toggle=True,
        metrics=[],
        metrics_type=2,
        metrics_nodes='worst',
        shuffle=False,
        save_test_to_file=True,
        test_folder_name_struct=[
            'test4',
            'dataset',
            'samp',
            'alpha',
            'nodes',
            # 'shuffle',
            'w_domain',
            'distr',
            # 'time',
            'iter',
            # 'metrics'
        ],
        test_parent_folder="",
        instant_plot=False,
        plots=['iter_time', 'cont_hinge_loss_iter', 'cont_hinge_loss_time'],
        save_plot_to_file=save_plots,
        plot_global_w=False,
        plot_node_w=False
    )


def test_on_eigvecsvm_dataset(
        seed=None,
        graph_type='expander',
        graphs_list=None,
        n=100,
        max_iter=None,
        alpha=0.1,
        time_distr_class=None,
        time_distr_param=None,
        metrics_nodes='all',
        alt_exp=False,
        alert=False,
        save_plots=False
):
    if max_iter is None:
        max_iter = 5000

    if alert:
        print('test_on_eigvecsvm_dataset()')
        print('n={}, distr={}, alpha={}'.format(
            col(n, 'yellow'),
            col(time_distr_class.shortname, 'cyan'),
            col(alpha, 'green')))
        input("click [ENTER] to continue or [CTRL]+[C] to abort")
        print(col("Simulation is preparing to start", 'green'))

    if metrics_nodes in ['worst', 'all']:
        metrics_type = {'worst': 2, 'all': 0, 'best': 2}[metrics_nodes]
    else:
        metrics_type = 2

    if graphs_list is None or len(graphs_list) == 0:
        graphs_list = get_graphs(graph_type, n)

    if alt_exp:
        dataset = 'alteigvecsvm'
    else:
        dataset = 'eigvecsvm'

    simulator.run(
        seed=seed,
        n=n,
        graphs=graphs_list,
        dataset=dataset,
        starting_weights_domain=[1, 1],
        smv_label_flip_prob=0.00,
        max_iter=max_iter,
        max_time=None,
        alpha=alpha,
        learning_rate='constant',
        spectrum_dependent_learning_rate=False,
        time_distr_class=time_distr_class,
        time_distr_param=time_distr_param,
        obj_function='cont_hinge_loss',
        epsilon=-math.inf,
        average_model_toggle=True,
        metrics=[],
        metrics_type=metrics_type,
        metrics_nodes=metrics_nodes,
        shuffle=False,
        save_test_to_file=True,
        test_folder_name_struct=[
            '3' + graph_type,
            'dataset',
            'alpha',
            'nodes',
            'shuffle',
            'w_domain',
            'distr',
            'time',
            'iter',
            'metrics'
        ],
        test_parent_folder="",
        instant_plot=False,
        plots=['iter_time', 'cont_hinge_loss_iter', 'cont_hinge_loss_time'],
        save_plot_to_file=save_plots,
        plot_global_w=False,
        plot_node_w=False
    )


def get_graphs(graph_type, nodes):
    """
    Generate the list of graphs from a graph type and the node amount
    using the degrees specified here inside the body of the function

    Parameters
    ----------
    graph_type : str
        Type of the graph to create the list which.
        For d=n-1 a clique is always created instead.

    nodes : int
        Nodes amount.

    Returns
    -------
    graph_list : List[str]
    """
    deg = {
        100: [2, 3, 4, 8, 16, 32, 64, 99],
        400: [2, 3, 4, 6, 20, 50, 100, 200, 300, 399],
        1000: [2, 3, 4, 8, 16, 20, 30, 40, 50, 100, 200, 500, 999]
    }[nodes]

    graph_list = []

    for d in deg:
        if d == nodes - 1:
            graph_list.append(str(d) + '-clique')
        else:
            graph_list.append(str(d) + '-' + graph_type)

    return graph_list


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Run test simulations'
    )

    parser.add_argument(
        'tests',
        type=str,
        nargs='*',
        help='Specify the names of the test functions to run',
        default=[]
    )

    parser.add_argument(
        '-s', '--seed',
        action='store',
        required=False,
        help='Simulation seed',
        dest='seed',
        type=int,
        default=None
    )

    parser.add_argument(
        '-d', '--distr',
        action='store',
        required=False,
        help='Time distribution (available distributions are: exp, par, unif, spark, asciq)',
        dest='distr',
        type=str,
        default='exp'
    )

    parser.add_argument(
        '-i', '--iter',
        action='store',
        required=False,
        help='Number of iterations to simulate',
        dest='max_iter',
        type=int,
        default=None
    )

    parser.add_argument(
        '-p', '--plot',
        action='store_true',
        required=False,
        help='Save plots into log folder as simulation finishes',
        dest='save_plots',
        default=False
    )

    args = parser.parse_args()

    run(args.tests, args.seed, args.max_iter, args.distr, args.save_plots)
